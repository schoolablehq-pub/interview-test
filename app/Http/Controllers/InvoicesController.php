<?php

namespace App\Http\Controllers;

use App\Http\Resources\InvoiceResource;
use App\Mail\InvoiceGenerated;
use App\Models\Customer;
use App\Models\Invoice;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class InvoicesController extends Controller
{
    public function store(Request $request)
    {


        $this->validate($request, [
            'customer.id' => ['required_without:customer.email,customer.name'],
            'customer.email' => ['required_without:customer.id'],
            'customer.name' => ['required_without:customer.id'],
            'invoice.title' => ['required'],
            'invoice.amount' => ['required', 'numeric'],
        ]);

        if($request->has('customer.id')) {
            $customer = Customer::find($request->input('customer.id'));
        } else {
            $customer = Customer::create([
                'email' => $request->input('customer.email'),
                'name' => $request->input('customer.name')
            ]);

            Http::post('https://webhook.site/07d8d18f-f20d-4c3d-aa92-8203f856ce11', [
                'event_type' => 'customers.new',
                'data' => $customer->toArray()
            ]);
        }

        $reference = $request->has('invoice.reference') ? $request->input('invoice.reference') : str_pad(Invoice::count() + 1, 5, '00000', STR_PAD_LEFT);

        $invoice = Invoice::create([
            'customer_id' => $customer->id,
            'title' => $request->input('invoice.title'),
            'reference' => $reference,
            'amount' => $request->input('invoice.amount')
        ]);

        Http::post('https://webhook.site/07d8d18f-f20d-4c3d-aa92-8203f856ce11', [
            'event_type' => 'invoices.new',
            'data' => $invoice->toArray()
        ]);

        $timestamp = time();
        $pdfFilePath = "pdfs/{$timestamp}-{$invoice->id}.pdf";

        $pdf = PDF::loadView('pdf_templates.invoices.new', ['invoice' => $invoice]);

        Storage::put($pdfFilePath, $pdf->output());

        Mail::to($customer->email)->send(new InvoiceGenerated($invoice, $pdfFilePath));

        return new InvoiceResource($invoice);
    }
}
